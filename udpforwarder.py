"""
udpforwarder.py -- a UDP forwarder / application-layer multicaster

Usage: python udpforwarder.py SPEAKERS_PORT LISTENERS_PORT 

Terms:
* forwarder: this program
* speaker: a source of UDP datagrams that should be forwarded to all
  receivers using the forwarder on a specific port
* listener: a sink for UDP datagrams that cannot receive these datagrams
  directly because it is behind a firewall or NAT gateway

Assumptions:
* speakers should work unchanged, besides using the forwarder's
  (instead of the receiver's) IP address and port as destinations
* listeners should have minimal setup overhead for firewall / NAT traversal
* listeners have means to discriminate packets besides the speaker's
  source address and port
* demultiplexing of multicast groups is done on forwarder port pairs:
  One port of a group is for speakers to this group, one port is for
  listeners in this group.

General idea:
* Speakers send datagrams to the speakers port and are done.
* The forwarder forwards these datagrams to the listeners on the
  listeners port associated with that speakers port.
* Since listeners must implicitly create state in their firewalls or
  NAT gateways to allow datagrams on the return path, they send a
  datgram to the forwarder (whose contents the forwarder ignores).
  The forwarder now knows the listener's IP address and port. It uses
  these to forward any traffic sourced by speakers to the listeners
* The forwarder has two listening sockets:
  - One for accepting incoming speakers, on the speaker port
  - One for accepting incoming listeners, on the listener port
* Incoming messages on the sockets are serviced in turns:
  - Every new message to the listeners port registers a new listener
  - Every new message to the speakers port makes the forwarder send out
    the message contents to all listeners
"""


# Should assumed-stale listeners be removed from the multicast tree?
DO_CLEANUP = True

# Wait this many seconds between cleanups
CLEANUP_INTERVAL = 60 * 15



import sys
import socket
import threading
import time
import traceback

listeners = set()
stale_listeners = set()



def set_up_listening_sockets(speakers_port, listeners_port):
    # Return sockets bound to the given ports
    speakers_socket = socket.socket(family=socket.AF_INET,
            type=socket.SOCK_DGRAM)
    listeners_socket = socket.socket(family=socket.AF_INET,
            type=socket.SOCK_DGRAM)

    speakers_socket.bind(("", speakers_port))
    listeners_socket.bind(("", listeners_port))

    speakers_socket.setblocking(True)
    listeners_socket.setblocking(True)

    return speakers_socket, listeners_socket



class Forward(threading.Thread):
    def __init__(self, speakers_socket, listeners_socket):
        threading.Thread.__init__(self)
        self.speakers_socket = speakers_socket
        self.listeners_socket = listeners_socket
    def run(self):
        while True:
            # Block until we receive a speaker message
            message = ""
            message, _ = self.speakers_socket.recvfrom(65536)

            try:
                for listener in listeners:
                    self.listeners_socket.sendto(message, listener)
                    print("Sent", message, "to", listener)
            except Exception as e:
                # This is a thread, we have nowhere to `raise` to.
                # print the error and continue
                print("forwarding_thread error:")
                traceback.print_exc()



class RegisterAndKeepalive(threading.Thread):
    def __init__(self, listeners_socket):
        threading.Thread.__init__(self)
        self.listeners_socket = listeners_socket
    def run(self):
        while True:
            try:
                # Remember incoming listeners, ignore their messages
                _, listener_address = self.listeners_socket.recvfrom(65536)
                listeners.add(listener_address)
                print("Ohai listener", listener_address)
                # We might know this listener. Count the message as
                # keepalive. This listener obviously isn't stale!
                stale_listeners.discard(listener_address)
            except Exception as e:
                # This is a thread, we have nowhere to `raise` to.
                # print the error and continue
                print("register_and_keepalive_thread error:")
                traceback.print_exc()



def start_forwarder(speakers_port, listeners_port):
    """
    Start the forwarder:
    * Listen on speaker and listener ports
    * Watch out for new listeners
    * Forward data from speakers to listeners
    """

    speakers_socket, listeners_socket = set_up_listening_sockets(
            speakers_port, listeners_port)

    forwarding_thread = Forward(speakers_socket, listeners_socket)
    forwarding_thread.start()

    register_and_keepalive_thread = RegisterAndKeepalive(listeners_socket)
    register_and_keepalive_thread.start()

    # Keep the listeners set fresh by timing out listeners
    # who don't send keepalive messages. For this, we just keep
    # a separate set of listeners we assume are stale until we
    # receive a keepalive message from them in the keepalive thread.
    # If they are stale, i.e. we don't receive keepalives, remove
    # them from the listerners set.
    while DO_CLEANUP:
        time.sleep(CLEANUP_INTERVAL)
        # Remove stale_listeners from listeners
        print("Removing stale listeners:", stale_listeners)
        listeners.difference_update(stale_listeners)
        # Start statekeeping anew
        stale_listeners.clear()
        stale_listeners.update(listeners)



if __name__ == "__main__":
    try:
        _, speakers_port, listeners_port = sys.argv
        start_forwarder(int(speakers_port), int(listeners_port))
    except Exception as e:
        raise
        print("Error:", e, "\n\nArguments: SPEAKERS_PORT LISTENERS_PORT")
        sys.exit(1)

